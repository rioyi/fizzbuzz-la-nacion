module.exports = (arr) => {
  const isValidArray = (arr) =>
    arr && arr.length && !arr.find((el) => typeof el === "string");

  if (!isValidArray(arr)) throw "Necesitas un arreglo de enteros";

  const validations = [
    {
      name: "FizzBuzz",
      condition: (el) => el % 3 === 0 && el % 5 === 0,
    },
    {
      name: "Fizz",
      condition: (el) => el % 3 === 0 || el.toString().includes(3),
    },
    {
      name: "Buzz",
      condition: (el) => el % 5 === 0 || el.toString().includes(5),
    },
    { name: "Default", condition: () => true },
  ];

  return arr.map((el) => validations.find((a) => a.condition(el)).name);
};
