const validator = require("./index");

describe("index", () => {
  describe("validator", () => {
    it("should return Error", () => {
      const ERROR_MSG = "Necesitas un arreglo de enteros";
      expect(() => validator()).toThrowError(ERROR_MSG);
      expect(() => validator([])).toThrowError(ERROR_MSG);
      expect(() => validator([1, 3, "a"])).toThrowError(ERROR_MSG);
    });

    it("should return Fizz", () => {
      expect(validator([9, 13])).toEqual(["Fizz", "Fizz"]);
    });

    it("should return Buzz", () => {
      expect(validator([10, 59])).toEqual(["Buzz", "Buzz"]);
    });

    it("should return FizzBuzz", () => {
      expect(validator([15, 30])).toEqual(["FizzBuzz", "FizzBuzz"]);
    });

    it("should return empty", () => {
      expect(validator([2, 1])).toEqual(["Default", "Default"]);
    });
  });
});
